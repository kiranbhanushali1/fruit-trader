package com.company.singleton;

import com.company.models.Fruit;

import java.util.HashMap;
import java.util.Map;

public class FruitLazySingleton {
    private static Map<String, Fruit>  fruitMap = new HashMap<>();

    public static Fruit getFruit(String fruitName){
        if( fruitMap.get(fruitName) == null){
            fruitMap.put(fruitName,new Fruit(fruitName) );
        }
        return fruitMap.get(fruitName);
    }

}
